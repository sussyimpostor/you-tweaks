### Note: I don't like the way You.com is currently going towards, so I decided to no longer maintain this userstyle. I'm now using Ecosia with the [Ecosia Modern theme](https://userstyles.world/style/5296/ecosia-modern) by Freeplay over You.com.
<div align="center"><img src="./youtweaks-logo.svg" alt="YouTweaks logo" style="border-radius:50%" width="400"/>
<br>A userstyle to make You more Yours.
</div>
<br>

# ℹ️ About

YouTweaks is a userstyle for You.com that allows you to customize your expierence more.

## ⚙️ All currently available options

- Clean the homepage 
- Clean the results page 
- Clean the top bar 
- Hide You.com app list at the top on homepage 
- Hide newsletter on footer 
- Hide like/dislike in search results 
- Hide 'Make default' in user dropdown 
- Make results look more consistent and nice
- Full width results for images, videos and news 
- Make results more compact 
- Make Private Mode use the same background color as Personal Mode 
- Search bar at the top 